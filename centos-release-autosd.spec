%global distro  Automotive Stream Distribution
%global major   9
%global minor   0

Name:       centos-release-autosd
Version:    %{major}.%{minor}
Release:    3%{?dist}
License:    GPLv2
URL:        https://sig.centos.org/automotive/
Summary:    AutoSD release file
Source0:    RPM-GPG-KEY-CentOS-SIG-Automotive
Source1:    CentOS-SIG-AutoSD.repo

Source300:    90-default.preset
Source301:    99-default-disable.preset

BuildArch:  noarch

Conflicts:  centos-stream-release
Provides:   centos-release
Provides:   centos-stream-release
Provides:   system-release
Provides:   system-release(releasever) = %{major}
Provides:   redhat-release

# required by epel-release
Provides:   redhat-release = %{version}-%{release}

# We still want to have the centos-stream repos and gpg keys
Requires:   centos-gpg-keys
Requires:   centos-stream-repos

%description
AutoSD release file and configs for the CentOS Automotive SIG repositories.
AutoSD repository provides also the packages usually included in CentOS Stream repositories.

%prep
# Nothing to do

%install
# Install the release files

# create skeleton
mkdir -p %{buildroot}/etc
mkdir -p %{buildroot}%{_prefix}/lib

# -------------------------------------------------------------------------
# Definitions for /etc/os-release and for macros in macros.dist.  These
# macros are useful for spec files where distribution-specific identifiers
# are used to customize packages.

# Name of vendor / name of distribution. Typically used to identify where
# the binary comes from in --help or --version messages of programs.
# Examples: gdb.spec, clang.spec
%global dist_vendor CentOS
%global dist_name   %{distro}

# URL of the homepage of the distribution
# Example: gstreamer1-plugins-base.spec
%global dist_home_url https://centos.org/

# Bugzilla / bug reporting URLs shown to users.
# Examples: gcc.spec
%global dist_bug_report_url https://bugzilla.redhat.com/

# debuginfod server, as used in elfutils.spec.
%global dist_debuginfod_url https://debuginfod.centos.org/
# -------------------------------------------------------------------------

# create /etc/system-release and /etc/redhat-release
install -d -m 0755 %{buildroot}%{_sysconfdir}
echo "%{distro} release %{major}" > %{buildroot}%{_sysconfdir}/centos-release
ln -s centos-release %{buildroot}%{_sysconfdir}/system-release
ln -s centos-release %{buildroot}%{_sysconfdir}/redhat-release

# Create the os-release file
install -d -m 0755 %{buildroot}%{_prefix}/lib
cat > %{buildroot}%{_prefix}/lib/os-release << EOF
NAME="%{distro}"
VERSION="%{major}"
ID="autosd"
ID_LIKE="rhel fedora centos"
VERSION_ID="%{major}"
PLATFORM_ID="platform:el%{major}"
PRETTY_NAME="%{distro} %{major}"
ANSI_COLOR="0;31"
LOGO="fedora-logo-icon"
CPE_NAME="cpe:/o:centos:autosd:%{major}"
HOME_URL="https://sig.centos.org/automotive/"
REDHAT_SUPPORT_PRODUCT="%{distro}"
REDHAT_SUPPORT_PRODUCT_VERSION="%{distro} %{major}"
EOF

# Create the symlink for /etc/os-release
ln -s ../usr/lib/os-release %{buildroot}%{_sysconfdir}/os-release

# write cpe to /etc/system/release-cpe
echo "cpe:/o:centos:autosd:%{major}" > %{buildroot}%{_sysconfdir}/system-release-cpe

# create /etc/issue, /etc/issue.net and /etc/issue.d
echo '\S' > %{buildroot}%{_sysconfdir}/issue
echo 'Kernel \r on an \m' >> %{buildroot}%{_sysconfdir}/issue
cp %{buildroot}%{_sysconfdir}/issue{,.net}
echo >> %{buildroot}%{_sysconfdir}/issue
mkdir -p %{buildroot}%{_sysconfdir}/issue.d

# set up the dist tag macros
mkdir -p %{buildroot}%{_rpmmacrodir}
cat > %{buildroot}%{_rpmmacrodir}/macros.dist << EOF
# dist macros.

%%__bootstrap ~bootstrap
%%centos_ver %{major}
%%centos %{major}
%%rhel %{major}
%%dist %%{!?distprefix0:%%{?distprefix}}%%{expand:%%{lua:for i=0,9999 do print("%%{?distprefix" .. i .."}") end}}.el%{major}%%{?distsuffix}%%{?with_bootstrap:%{__bootstrap}}
%%el%{major} 1
%%dist_vendor         %{dist_vendor}
%%dist_name           %{dist_name}
%%dist_home_url       %{dist_home_url}
%%dist_bug_report_url %{dist_bug_report_url}
%%dist_debuginfod_url %{dist_debuginfod_url}
EOF

# Install the repo files
install -d %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -p -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -d %{buildroot}%{_sysconfdir}/yum.repos.d
install -p -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/yum.repos.d

# copy systemd presets
install -d -m 0755 %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -p -m 0644 %{SOURCE300} %{buildroot}%{_prefix}/lib/systemd/system-preset/

# installing the same file for both system and user presets to set the same behavior for both
install -d -m 0755 %{buildroot}%{_prefix}/lib/systemd/user-preset/
install -p -m 0644 %{SOURCE301} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -p -m 0644 %{SOURCE301} %{buildroot}%{_prefix}/lib/systemd/user-preset/

%files
%defattr(-,root,root)
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-Automotive
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-SIG-AutoSD.repo
%{_sysconfdir}/redhat-release
%{_sysconfdir}/system-release
%{_sysconfdir}/centos-release
%config(noreplace) %{_sysconfdir}/os-release
%config %{_sysconfdir}/system-release-cpe
%config(noreplace) %{_sysconfdir}/issue
%config(noreplace) %{_sysconfdir}/issue.net
%{_rpmmacrodir}/macros.dist
%{_prefix}/lib/os-release
%{_prefix}/lib/systemd/system-preset/*
%{_prefix}/lib/systemd/user-preset/*

%changelog
* Wed Jul 03 2024 Sandro Bonazzola <sbonazzo@redhat.com> - 9-3
- Fix URL as the old wiki is now archived
- Fix gpgkeys repo configuration as the autosd repository packages are coming from two different build systems and signed with two different keys

* Wed Aug 16 2023 Michael Ho <micho@redhat.com> - 9-2
- Remove automotive-sig repos
- Change conflicts/provides to remove centos-release-automotive
- Rename AutoSD.repo to CentOS-SIG-AutoSD.repo

* Tue Jun 13 2023 Michael Ho <micho@redhat.com> - 9-1
- Initial fork of centos-release-automotive package for AutoSD
